class AfterRegisterController < ApplicationController
  include Wicked::Wizard

   steps :step_three, :step_four

   def show
     render_wizard
   end
end
